package com.basesoft;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ListAssertion<T, ASSERTION_TYPE> {

    private final List<T> row;
    private final Function<T, ASSERTION_TYPE> toAssertion;

    public ListAssertion(List<T> row, Function<T, ASSERTION_TYPE> toAssertion) {
        this.row = row;
        this.toAssertion = toAssertion;
    }

    public ASSERTION_TYPE at(Integer index){
        return toAssertion.apply(row.get(index));
    }

    public ASSERTION_TYPE first(){
        return at(0);
    }

    public ListAssertion<T, ASSERTION_TYPE> match(Predicate<T> predicate){
        return new ListAssertion<>(row.stream()
           .filter(predicate)
           .collect(Collectors.toList()),
           toAssertion
        );
    }
}
