package com.basesoft.pfm;

import com.basesoft.pfm.domain.cashflow.CashFlowView;
import com.basesoft.pfm.domain.cashflow.model.Money;
import com.basesoft.pfm.domain.cashflow.model.TransactionType;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class CashFlowViewAssertion {

    private CashFlowView cashFlowView;

    public CashFlowViewAssertion(CashFlowView cashFlowView) {
        this.cashFlowView = cashFlowView;
    }

    public CashFlowViewAssertion hasAmount(Money money) {
        assertThat(cashFlowView.getAmount()).isEqualTo(money);
        return this;
    }

    public CashFlowViewAssertion createAt(Date now) {
        assertThat(cashFlowView.getCreatedAt()).hasSameTimeAs(now);
        return this;
    }

    public CashFlowViewAssertion isExpense() {
        assertThat(cashFlowView.getType()).isEqualTo(TransactionType.OUTCOME);
        return this;
    }
}
