package com.basesoft.pfm;

import com.basesoft.ListAssertion;
import com.basesoft.pfm.domain.cashflow.CashFlowView;

import java.util.List;

public class CashFlowViewListAssertion extends ListAssertion<CashFlowView, CashFlowViewAssertion> {

    public CashFlowViewListAssertion(List<CashFlowView> row) {
        super(row, CashFlowViewAssertion::new);
    }

    public static CashFlowViewListAssertion assertListTransactionView(List<CashFlowView> all) {
        return new CashFlowViewListAssertion(all);
    }
}
