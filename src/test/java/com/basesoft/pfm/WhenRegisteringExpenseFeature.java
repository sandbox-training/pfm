package com.basesoft.pfm;

import com.basesoft.PfmApplicationTests;
import com.basesoft.pfm.domain.cashflow.FindCashFlow;
import com.basesoft.pfm.domain.cashflow.CashFlowUseCase;
import com.basesoft.pfm.domain.cashflow.model.Money;
import com.basesoft.pfm.infrastructure.cashflow.JpaCashFlowRepository;
import com.basesoft.pfm.infrastructure.cashflow.CashFlowAssembler;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.ZoneId;
import java.util.Date;

import static com.basesoft.pfm.CashFlowViewListAssertion.assertListTransactionView;

public class WhenRegisteringExpenseFeature extends PfmApplicationTests{

    public static Date NOW = new Date();

    @Autowired
    private JpaCashFlowRepository transactionRepository;

    @Autowired
    private JpaTransactionManager txManager;

    @Autowired
    private FindCashFlow findCashFlow;

    private CashFlowUseCase cashFlowUseCase;

    @Before
    public void setUp() throws Exception {
        final CashFlowAssembler assembler = new CashFlowAssembler();
        cashFlowUseCase = assembler.transactionUseCases(transactionRepository, Clock.fixed(NOW.toInstant(), ZoneId
                .systemDefault()));
    }

    @Test
    public void shouldAddNewExpenseOnListing(){
        //given
        BigDecimal amount = new BigDecimal(1000);

        //when
        doInTransaction(() -> cashFlowUseCase.addExpense(amount));

        //then
        assertListTransactionView(findCashFlow.all()).first()
                                                     .hasAmount(new Money(amount))
                                                     .createAt(NOW)
                                                     .isExpense();

    }


    public void doInTransaction(Runnable runnable){
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        txTemplate.execute(status -> {
            runnable.run();
            return null;
        });
    }



}
