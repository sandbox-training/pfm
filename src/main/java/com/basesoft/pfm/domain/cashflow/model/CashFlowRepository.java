package com.basesoft.pfm.domain.cashflow.model;

public interface CashFlowRepository {
    CashFlowId nextId();
    void save(CashFlow expense);
    CashFlow find(CashFlowId id);
}
