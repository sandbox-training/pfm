package com.basesoft.pfm.domain.cashflow;

import com.basesoft.pfm.domain.cashflow.model.CashFlowId;
import com.basesoft.pfm.domain.cashflow.model.TransactionType;
import com.basesoft.pfm.domain.cashflow.model.Money;

import java.util.Date;

public class CashFlowView {

    private CashFlowId cashFlowId;
    private Money amount;
    private Date createdAt;
    private TransactionType type;

    public CashFlowView() {
        //for reflection purpose only!
    }

    public Money getAmount() {
        return amount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public TransactionType getType() {
        return type;
    }

    public CashFlowId getCashFlowId() {
        return cashFlowId;
    }

    public void setCashFlowId(CashFlowId cashFlowId) {
        this.cashFlowId = cashFlowId;
    }

    public void setAmount(Money amount) {
        this.amount = amount;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }
}
