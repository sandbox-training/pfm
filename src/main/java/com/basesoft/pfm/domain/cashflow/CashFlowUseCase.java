package com.basesoft.pfm.domain.cashflow;

import com.basesoft.pfm.domain.cashflow.model.Money;
import com.basesoft.pfm.domain.cashflow.model.CashFlow;
import com.basesoft.pfm.domain.cashflow.model.CashFlowRepository;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Clock;

public class CashFlowUseCase {

    private CashFlowRepository cashFlowRepository;
    private Clock clock;

    public CashFlowUseCase(CashFlowRepository cashFlowRepository, Clock clock) {
        this.cashFlowRepository = cashFlowRepository;
        this.clock = clock;
    }

    public void addExpense(BigDecimal amount){
        final Money cost = new Money(amount);
        final CashFlow expense = CashFlow.expense(
                cashFlowRepository.nextId(),
                cost,
                Date.from(clock.instant())
        );
        cashFlowRepository.save(expense);
    }
}
