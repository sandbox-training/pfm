package com.basesoft.pfm.domain.cashflow.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Transaction")
public class CashFlow {

    @EmbeddedId
    private CashFlowId cashFlowId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Enumerated
    private TransactionType type;

    @Embedded
    private Money amount;

    protected CashFlow() {
        //for reflection purpose only!
    }

    private CashFlow(CashFlowId cashFlowId, TransactionType type, Money amount, Date createdAt) {
        this.cashFlowId = cashFlowId;
        this.type = type;
        this.amount = amount;
        this.createdAt = createdAt;
    }


    public static CashFlow expense(CashFlowId cashFlowId, Money amount, Date createdAt) {
        return new CashFlow(cashFlowId, TransactionType.OUTCOME, amount, createdAt);
    }
}
