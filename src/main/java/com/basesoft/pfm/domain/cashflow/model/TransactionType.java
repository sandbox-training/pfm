package com.basesoft.pfm.domain.cashflow.model;

public enum TransactionType {
    INCOME,OUTCOME;
}
