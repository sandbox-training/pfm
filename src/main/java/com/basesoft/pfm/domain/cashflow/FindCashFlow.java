package com.basesoft.pfm.domain.cashflow;

import java.util.List;

public interface FindCashFlow {
    List<CashFlowView> all();
}
