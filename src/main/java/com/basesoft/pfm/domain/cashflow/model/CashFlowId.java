package com.basesoft.pfm.domain.cashflow.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CashFlowId implements Serializable {
    private String id;

    public CashFlowId(String id) {
        this.id = id;
    }

    protected CashFlowId() {
        //for reflection purpose only
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CashFlowId that = (CashFlowId) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
