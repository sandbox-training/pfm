package com.basesoft.pfm.infrastructure;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.basesoft.pfm.infrastructure")
public class PfmConfiguration {
}
