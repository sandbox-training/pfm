package com.basesoft.pfm.infrastructure.cashflow;

import com.basesoft.pfm.domain.cashflow.model.CashFlow;
import com.basesoft.pfm.domain.cashflow.model.CashFlowId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaCashFlowRepository extends CrudRepository<CashFlow, CashFlowId> {
}
