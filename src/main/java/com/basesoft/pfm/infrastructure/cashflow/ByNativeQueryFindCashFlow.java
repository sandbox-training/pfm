package com.basesoft.pfm.infrastructure.cashflow;

import com.basesoft.pfm.domain.cashflow.FindCashFlow;
import com.basesoft.pfm.domain.cashflow.CashFlowView;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

class ByNativeQueryFindCashFlow implements FindCashFlow {

    private DataSource dataSource;

    public ByNativeQueryFindCashFlow(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<CashFlowView> all() {
        return new NamedParameterJdbcTemplate(dataSource)
                .query("select * from Transaction", new MapSqlParameterSource(), new BeanPropertyRowMapper<>
                        (CashFlowView.class));
    }
}
