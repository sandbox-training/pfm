package com.basesoft.pfm.infrastructure.cashflow;

import com.basesoft.pfm.domain.cashflow.model.CashFlow;
import com.basesoft.pfm.domain.cashflow.model.CashFlowId;
import com.basesoft.pfm.domain.cashflow.model.CashFlowRepository;

import java.util.UUID;

class CashFlowRepositoryFacade implements CashFlowRepository {

    private JpaCashFlowRepository jpaCashFlowRepository;

    public CashFlowRepositoryFacade(JpaCashFlowRepository jpaCashFlowRepository) {
        this.jpaCashFlowRepository = jpaCashFlowRepository;
    }

    @Override
    public CashFlowId nextId() {
        return new CashFlowId(UUID.randomUUID().toString());
    }

    @Override
    public void save(CashFlow expense) {
        jpaCashFlowRepository.save(expense);
    }

    @Override
    public CashFlow find(CashFlowId id) {
        return jpaCashFlowRepository.findOne(id);
    }
}
