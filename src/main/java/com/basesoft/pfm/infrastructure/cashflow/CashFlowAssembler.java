package com.basesoft.pfm.infrastructure.cashflow;

import com.basesoft.pfm.domain.cashflow.FindCashFlow;
import com.basesoft.pfm.domain.cashflow.CashFlowUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.time.Clock;

@Configuration
public class CashFlowAssembler {

    @Bean
    public CashFlowUseCase transactionUseCases(JpaCashFlowRepository jpaCashFlowRepository, Clock clock){
        final CashFlowRepositoryFacade transactionRepository = new CashFlowRepositoryFacade(jpaCashFlowRepository);
        final CashFlowUseCase cashFlowUseCase = new CashFlowUseCase(transactionRepository, clock);
        return cashFlowUseCase;
    }

    @Bean
    public FindCashFlow findTransaction(DataSource dataSource){
        return new ByNativeQueryFindCashFlow(dataSource);
    }

    @Bean
    public Clock clock(){
        return Clock.systemDefaultZone();
    }

}
